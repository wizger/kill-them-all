﻿using UnityEngine;
using System.Collections;

public class destruirVida : MonoBehaviour {
	float tiempo;
	// Use this for initialization
	void Start () {
		tiempo = Time.time + 5f;
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time > tiempo)
			Destroy(gameObject);
	}

	void OnTriggerExit2D(Collider2D col){
	}
	
	void OnTriggerStay2D(Collider2D col){
		if (col.gameObject.name == "gato"){
			col.gameObject.SendMessage("subirVida",10);
			Destroy(gameObject);
		}
	}


}
