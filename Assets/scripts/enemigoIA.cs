﻿using UnityEngine;
using System.Collections;

public class enemigoIA : MonoBehaviour {
	public GameObject corazon;
	public GameObject poder;
	GameObject target;
	float vida;
	Animator anim;
	bool destruir;
	bool setTime,puedeMor;
	double tiempo;
	static int numEnemigos,enemigosMuertos;
	bool mordiendo;
	float tiempoMor;
	//movJugador jugador;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		vida = 10;
		destruir = false;
		setTime = true;
		target = GameObject.Find ("gato");
		mordiendo = false;
		tiempoMor = Time.time + 4.4f;
		puedeMor = false;
	}
	
	// Update is called once per frame
	void Update () {
		mordiendo = false;
		if(Time.time > tiempoMor)
			puedeMor = true;
		else
			puedeMor = false;
		//print(gameObject.tag);
		anim.SetInteger ("etapa",1);
//		transform.position += (target.transform.position - transform.position).normalized * 1 * Time.deltaTime;
		if (Vector3.Distance(transform.position,target.transform.position)>=0.8f){//move if distance from target is greater than 1
			transform.position += (target.transform.position - transform.position).normalized * 1 * Time.deltaTime;
			if (transform.position.x > target.transform.position.x) {
				transform.rotation = Quaternion.Euler (0, 180, 0);
			} else if(transform.position.x < target.transform.position.x){
				transform.rotation = Quaternion.Euler(0,360,0);
			}
		}else{
			mordiendo = true;
			anim.SetInteger("etapa",0);
		}
		if (vida <= 0) {
			anim.SetInteger("etapa",2);
			if(setTime){
				tiempo = Time.time + 0.5;
				destruir = true;
				setTime = false;
				enemigosMuertos++;
				resCant();
			}
		}
		if(destruir){
			if(Time.time >= tiempo){
				if(Random.Range(1,20) == 4){//aqui es la prob de que salga un corazon
					Vector3 position = new Vector3(transform.position.x + (transform.localScale.x/3),transform.position.y);
					Instantiate(corazon,position,Quaternion.identity);
				}else if(Random.Range(1,20) == 5){//aqui es la prob de que salga una bateria
					Vector3 position = new Vector3(transform.position.x + (transform.localScale.x/3),transform.position.y);
					Instantiate(poder,position,Quaternion.identity);
				}
			Destroy(gameObject);
			}
		}

	}

	public void setVida(float cantidad){
		vida -= cantidad;
	}

	public void sumCant(){
		numEnemigos++;
	}

	public void resCant(){
		numEnemigos--;
	}

	public int getCant(){
		return numEnemigos;
	}

	public int getEneMuer(){return enemigosMuertos;}
	public void setEneMuer(){enemigosMuertos = 0;}
	public void setNumEne(){numEnemigos = 0;}

	void OnTriggerExit2D(Collider2D col){
	}
	
	void OnTriggerStay2D(Collider2D col){
		if(puedeMor)
		if (col.gameObject.name == "gato") {
			Debug.Log((int)transform.rotation.y != (int)col.transform.rotation.y);
			if((int)transform.rotation.y != (int)col.transform.rotation.y){
				if(Time.time > tiempoMor){
					if(mordiendo){
						col.gameObject.SendMessage("setVida",5);
						tiempoMor = Time.time + 4.4f;
					}
				}	
			}else{
				if(Time.time > tiempoMor){
					if(mordiendo){
						col.gameObject.SendMessage("setVida",5);
						tiempoMor = Time.time + 4.4f;
					}
				}
			}
		}
	}


}
