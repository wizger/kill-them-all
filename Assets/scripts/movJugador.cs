﻿using UnityEngine;
using System.Collections;

public class movJugador : MonoBehaviour {
	public GameObject proyectilPreFab;
	Animator anim;
	int rotarPoder,animNum,enemigosMuertos;
	bool golpe;
	bool arriba;
	bool derecha;
	bool izquierda;
	bool abajo;
	bool gFinal;
	enemigoIA enemigo;
	Rvar fin;
	bool puedeAbajo;
	bool puedeArriba;
	bool puedeDerecha;
	bool puedeIzquierda;
	bool tocandoEne,acumulador;
	float vida;
	float gameOverTiempo;
	bool canGolp; //esta era static
	enemigoIA datosEnemigo;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		canGolp = acumulador = false;
		golpe = false;
		arriba = false;
		derecha = false;
		izquierda = false;
		abajo = false;
		gFinal = false;
		puedeAbajo = true;
		puedeArriba = true;
		puedeDerecha = true;
		puedeIzquierda = true;
		rotarPoder = 0;
		tocandoEne = false;
		vida = 30;
		datosEnemigo = new enemigoIA();
		fin = new Rvar();
		animNum = enemigosMuertos = 0;
	}

	void OnGUI(){
		GUI.Box (new Rect(10,10, 0.03f * Screen.width * vida, 20), vida + "/" + 30);    
	}

	// Update is called once per frame
	void Update () {
		if (animNum == 0) {
			tocandoEne = false;
			//aqui va un gran if si animNum ==6 no hacer nada :D
			anim.SetInteger	 ("estado", 0);
			tocandoEne = false;
			if(derecha){
				if(puedeDerecha){
					transform.position = new Vector3(transform.position.x + (Time.deltaTime*2),transform.position.y);
					puedeIzquierda = true;
					rotarPoder = 0;
				}
				anim.SetInteger("estado",1);
				transform.rotation = Quaternion.Euler(0,0,0);
			}
			
			if(izquierda){
				if(puedeIzquierda){
					transform.position = new Vector3(transform.position.x + (Time.deltaTime*-2),transform.position.y);
					puedeDerecha = true;
					rotarPoder = 1;
				}
				anim.SetInteger("estado",1);
				transform.rotation = Quaternion.Euler(0,180,0);
			}
			
			if(arriba){
				if(puedeArriba){
					transform.position = new Vector3(transform.position.x,transform.position.y + (Time.deltaTime*2));
					puedeAbajo = true;
				}
				anim.SetInteger("estado",1);
			}
			
			if(abajo){
				if(puedeAbajo){
					transform.position = new Vector3(transform.position.x,transform.position.y + (Time.deltaTime*-2));
					puedeArriba = true;
				}
				anim.SetInteger("estado",1);
			}
			if(golpe){
				anim.SetInteger("estado",3);
				golpe=false;
				tocandoEne = true;
				audio.Play();
			}
			if((enemigosMuertos+8)== datosEnemigo.getEneMuer()){
				enemigosMuertos = datosEnemigo.getEneMuer();
				acumulador = true;
			}
			
			if(gFinal){
				if(acumulador){
					anim.SetInteger("estado",4);
					if(proyectilPreFab != null){
						int pos = rotarPoder==0? 1:-1;
						Vector3 position = new Vector3(transform.position.x + (pos *(transform.localScale.x/3)),transform.position.y);
						((destruirEGato)proyectilPreFab.GetComponent(typeof(destruirEGato))).rotar = transform.rotation;
						Instantiate(proyectilPreFab,position,Quaternion.identity);
					}
					canGolp = acumulador = false;
				}
				gFinal = false;
			}
		}else{
			if (anim.GetInteger ("estado") == 10)
				fin.destruir();
			if(Time.time >= gameOverTiempo){
				anim.SetInteger("estado",10);
			}
		}

		if(vida >=-20 && vida <=0){
			animNum = 6;
			anim.SetInteger("estado",6);
			vida = -21;
			gameOverTiempo = Time.time+ 1f;
		}

	}


	public void moverArriba(){
		arriba = true;
	}
	
	public void altoArriba(){
		arriba = false;
	}
	
	public void moverDerecha(){
		derecha = true;
	}
	
	public void altoDerecha(){
		derecha = false;
	}
	
	public void moverIzquierda(){
		izquierda = true;
	}
	
	public void altoIzquierda(){
		izquierda = false;
	}
	
	public void moverAbajo(){
		abajo = true;
	}
	
	public void altoAbajo(){
		abajo = false;
	}
	
	public void pegar(){
		golpe = true;
	}
	
	public void especial(){
		//if(canGolp)
			gFinal = true;
	}

	void OnCollisionEnter2D(Collision2D coll) {
	}

	void OnTriggerEnter2D(Collider2D col)
	{	
		if (col.gameObject.name == "boloide(Clone)") {
			if(tocandoEne){
				if((int)transform.rotation.y != (int)col.transform.rotation.y)
					col.gameObject.SendMessage("setVida",5);
			}
		}else if(col.gameObject.name == "abajo"){
			puedeAbajo = false;
		}else if(col.gameObject.name == "arriba"){
			puedeArriba = false;
		}else if(col.gameObject.name == "tope"){
			puedeIzquierda = false;
		}else if(col.gameObject.name == "topeFinal"){
			puedeDerecha = false;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.gameObject.name == "boloide(Clone)") {
			if(tocandoEne){
				Debug.Log((int)transform.rotation.y != (int)col.transform.rotation.y);
				if((int)transform.rotation.y != (int)col.transform.rotation.y)
					col.gameObject.SendMessage("setVida",10);
			}
		}
	}

	void setVida(float cantidad){
		vida -=cantidad;
	}

	void subirVida(float cantidad){
		if((vida > 0) && (vida <30))
			if( (vida + cantidad) >30)
				vida = 30;
			else
			vida += cantidad;
	}



}
