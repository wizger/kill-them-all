﻿using UnityEngine;
using System.Collections;

public class camAJugador : MonoBehaviour {
	public GameObject enemigo;
	float velCam;
	Vector3 velocidad;
	public Transform movJugador;
	Vector3 dimensionPan;
	// Use this for initialization
	void Start () {
		velCam = 0.3f;
		velocidad = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 punto = camera.WorldToViewportPoint(movJugador.position);
		Vector3 delta = movJugador.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, punto.z));
		Vector3 destino = transform.position + delta;
		destino.y = 0.17f;
		transform.position = Vector3.SmoothDamp(transform.position, destino, ref velocidad, velCam);
		//transform.position.x = Mathf.Clamp(transform.position.x, 6.9f, 23.8f);
		transform.position = new Vector3 (Mathf.Clamp(transform.position.x, 6.9f, 23.8f),transform.position.y,-10);

	
	}
}
