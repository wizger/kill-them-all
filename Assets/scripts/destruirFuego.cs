﻿using UnityEngine;
using System.Collections;

public class destruirObjeto : MonoBehaviour {
	private bool setTime = true;
	private float tiempo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.left* Time.deltaTime );
		transform.rotation = Quaternion.Euler (0, 180, 0);
		if (setTime) {
			tiempo = Time.time+ 5;
			setTime = false;
		}
		if (Time.time > tiempo) {
			Destroy (gameObject);
		}
	}
}
